using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Text;
using System.Security.Cryptography;

namespace Crypto
{
    /// <summary>
    /// Simple symmetric encryption using the .NET Rijndael provider
    /// </summary>
    public class SymmetricEncryption
    {
        public string Password
        {
            set { GenerateKey(value); }
        }

        private byte[] Key;
        private byte[] Vector;

        public SymmetricEncryption()
        {
        }

        public SymmetricEncryption(string password)
        {
            GenerateKey(password);
        }

        private void GenerateKey(string password)
        {
            SHA384Managed sha = new SHA384Managed();
            byte[] b = sha.ComputeHash(new ASCIIEncoding().GetBytes(password));

            Key = new byte[32];
            Vector = new byte[16];

            Array.Copy(b, 0, Key, 0, 32);
            Array.Copy(b, 32, Vector, 0, 16);
        }

        public string Encrypt(string plainText, string password)
        {
            GenerateKey(password);
            return Encrypt(plainText);
        }

        public byte[] Encrypt(byte[] data, string password)
        {
            GenerateKey(password);
            return Encrypt(data);
        }

        public string Encrypt(string plainText)
        {
            byte[] data = new ASCIIEncoding().GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(data));
        }

        public byte[] Encrypt(byte[] data) // string plainText)
        {
            if (Key == null)
            {
                throw new Exception("Key was not provided");
            }

            RijndaelManaged crypto = new RijndaelManaged();
            ICryptoTransform encryptor = crypto.CreateEncryptor(Key, Vector);

            MemoryStream memoryStream = new MemoryStream();
            CryptoStream crptoStream = new CryptoStream(memoryStream, 
                                                        encryptor, 
                                                        CryptoStreamMode.Write);

            crptoStream.Write(data, 0, data.Length);
            crptoStream.FlushFinalBlock();

            crptoStream.Close();
            memoryStream.Close();

            return memoryStream.ToArray();
        }

        public string Decrypt(string encryptedText, string password)
        {
            GenerateKey(password);
            return Decrypt(encryptedText);
        }

        public byte[] Decrypt(byte[] encryptedData, string password)
        {
            GenerateKey(password);
            return Decrypt(encryptedData);
        }

        public string Decrypt(string encryptedText)
        {
            byte[] cipher = Convert.FromBase64String(encryptedText);
            byte[] data = Decrypt(cipher);
            return (new ASCIIEncoding()).GetString(data, 0, data.Length);
        }

        public byte[] Decrypt(byte[] cipher)
        {
            if (Key == null)
            {
                throw new Exception("Key was not provided");
            }

            RijndaelManaged crypto = new RijndaelManaged();
            ICryptoTransform encryptor = crypto.CreateDecryptor(Key, Vector);

            MemoryStream memoryStream = new MemoryStream(cipher);
            CryptoStream crptoStream = new CryptoStream(memoryStream, 
                                                        encryptor, 
                                                        CryptoStreamMode.Read);

            byte[] data = new byte[cipher.Length];
            int dataLength = crptoStream.Read(data, 0, data.Length);

            memoryStream.Close();
            crptoStream.Close();

            byte[] result = new byte[dataLength];
            Buffer.BlockCopy(data, 0, result, 0, dataLength);
            return result;
        }
    }
    public class UEI
    {
        private const int INIT_CTR = 03; // was 57 - wrapped to 3 anyway

        static byte[] key = {   0x01, 0x37, 0x39, 0x35, 0x34, 0x6E, 0x64, 0x73, 0x66, //  - converted
                                0x6A, 0x6B, 0x68, 0x61, 0x6B, 0x6C, 0x66, 0x64, 0x6A,
                                0x6B, 0x6C, 0x34, 0x65, 0x37, 0x39, 0x38, 0x09, 0x34,
                                0x20, 0x2E, 0x22, 0x7E, 0x6C, 0x66, 0x0A, 0x6A, 0x68,
                                0x66, 0x6B, 0x68, 0x67, 0x38, 0x37, 0x34, 0x39, 0x38,
                                0x68, 0x74, 0x51, 0x51, 0x34, 0x32, 0x32, 0x39, 0x33};

        //Note: key4String can NOT include "\xfe" and "\xde" two characters
        //      because they are valid chars for intron
        static char[] key4String = { 
            '\x80', '\x81', '\x92', '\x9c', '\x82', '\x8e', '\xa0', '\xab', '\x83',
            '\x84', '\xa5', '\xd3', '\xca', '\x85', '\x95', '\x97', '\x86', '\x9e',
            '\x87', '\xb0', '\xd4', '\xd0', '\x88', '\xc8', '\xcf', '\xd9', '\x89',
            '\x86', '\x8b', '\x91', '\xb3', '\xb5', '\xb6', '\xda', '\xac', '\xdb',
            '\xc9', '\xaa', '\xb2'};

        // !!! Set 'pos' to -1 for normal use !!!
        public static void encr(ref byte[] buf, int buflen, int pos)
        {
            int buf_ctr = 0;
            int enCounter;
            int keylen = key.Length;

            enCounter = (pos >= 0) ? pos : INIT_CTR;

            while (buf_ctr < buflen)
                buf[(int)buf_ctr] = (byte)((buf[(int)buf_ctr++]) ^
                    (key[(enCounter++) % keylen]));
        }

        // !!! Set 'pos' to -1 for normal use !!!
        public static void encrString(ref char[] buf, int buflen, int pos)
        {
            int buf_ctr = 0;
            int enCounter;
            int len = key4String.Length;

            enCounter = (pos >= 0) ? pos : INIT_CTR;

            while (buf_ctr < buflen)
                buf[(int)buf_ctr] = (char)((buf[(int)buf_ctr++]) ^
                    (key4String[(enCounter++) % len]));
        }
    }
}

